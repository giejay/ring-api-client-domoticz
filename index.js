const ringApi = require('ring-api');
const colors = require('colors/safe');
const request = require('request');

// Make sure these env vars are set, through docker or when manually running the script:
const username = process.env.EMAIL;
const password = process.env.PASSWORD;
const domoticzUrl = process.env.DOMOTICZ_URL;
const ringDomoticzId = process.env.DOMOTICZ_RING_DEVICE_ID;
const motionDomoticzId = process.env.DOMOTICZ_MOTION_DEVICE_ID;
const domoticzIdForType = {'motion': motionDomoticzId, 'ding': ringDomoticzId};

let callAPI = function (deviceId, activityKind) {
    request(domoticzUrl + '/json.htm?type=command&param=switchlight&idx=' + deviceId + '&switchcmd=On', {json: true}, (err, res, body) => {
        if (err) {
            return console.log(err);
        }
        if(activityKind === 'ding'){
            console.log(body);
        }
    });
};
const main = async () => {

    let ring;
    try {
        ring = await ringApi({
            email: username, password: password
        })
    } catch (e) {
        console.error(e);
        console.error(colors.red('We couldn\'t create the API instance. This might be because ring.com changed their API again'))
        console.error(colors.red('or maybe your password is wrong, in any case, sorry can\'t help you today. Bye!'))
        return
    }

    try {
        ring.events.on('activity', activity => {
            console.log('event: ' + activity.kind, activity);
            let domoticzId = domoticzIdForType[activity.kind];
            if(domoticzId){
                callAPI(domoticzId, activity.kind)
            } else {
                console.log('Unknown activity type: ' + activity.kind);
            }
        });
        console.log('now listening for dings, they will log here until you kill this script. Go press your doorbell!')
    } catch (e) {
        console.error(e)
    }
};

main();
