Library for listening to Ring doorbell events and sending them to Domoticz. 

When using docker-compose you can use the following configuration:

```
ring-domoticz:
      image: giejay/ring-client-domoticz
      restart: always
      environment:
        - DOMOTICZ_URL=http://localhost:8080
        - EMAIL=youremail@email.com
        - PASSWORD=mypassword
        - DOMOTICZ_RING_DEVICE_ID=256
        - DOMOTICZ_MOTION_DEVICE_ID=257
```