FROM arm32v7/node:8-slim

COPY index.js /index.js
COPY package.json /package.json
WORKDIR /
RUN npm install

CMD ["node", "index.js"]
